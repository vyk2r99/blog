class Comment < ActiveRecord::Base
	belongs_to :post
	validates_presense_of :post_id
	validates_presense_of :body
end
